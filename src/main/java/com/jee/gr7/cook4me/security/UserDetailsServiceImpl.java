package com.jee.gr7.cook4me.security;

import com.jee.gr7.cook4me.exception.UserException;
import com.jee.gr7.cook4me.users.UserRepository;
import com.jee.gr7.cook4me.users.Users;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) {
        Users user = null;
        try {
            user = userRepository.findByUsername(username)
                    .orElseThrow(() -> new UserException("User Not Found with username: " + username));
        } catch (UserException e) {
            Logger.getLogger(UserDetailsServiceImpl.class).error(e.getMessage());
        }

        assert user != null;
        return UserDetailsImpl.build(user);
    }
}
