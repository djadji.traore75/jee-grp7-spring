package com.jee.gr7.cook4me;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Cook4meApplication {

    public static void main(String[] args) {
        SpringApplication.run(Cook4meApplication.class, args);
    }

}
