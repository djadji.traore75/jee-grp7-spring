package com.jee.gr7.cook4me.exception;

public class UserException extends Exception {

    public UserException(String message) {
        super(message);
    }
}
