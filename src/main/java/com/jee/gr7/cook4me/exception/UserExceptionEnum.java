package com.jee.gr7.cook4me.exception;

public enum UserExceptionEnum {
    USERDOESNTEXIST("User doesnt exist "),
    NOCMDFOUNDFORUSER("No command found for this user"),
    USERREGISTEREDSUCCESFULLY("User registered successfully !"),
    USERDELETEDSUCCESFULLY("User delete successfully !"),
    USERNAMETAKEN("Username already taken : "),
    INVALIDPHONENUMBER("This isnt a valid phone number : "),
    INVALIDEMAIL("This isnt an valid mail adress : "),
    MAILTAKEN("Mail already taken : ");

    String name;

    UserExceptionEnum(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
