package com.jee.gr7.cook4me.exception;

public class DishesException extends Exception {

    public DishesException() {
    }

    public DishesException(String message) {
        super(message);
    }
}
