package com.jee.gr7.cook4me.exception;

public class CatererException extends Exception {
    public CatererException(String message) {
        super(message);
    }
}
