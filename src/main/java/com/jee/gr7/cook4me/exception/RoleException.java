package com.jee.gr7.cook4me.exception;

public class RoleException extends Exception {

    public RoleException(String message) {
        super(message);
    }
}
