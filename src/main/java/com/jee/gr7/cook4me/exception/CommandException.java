package com.jee.gr7.cook4me.exception;

public class CommandException extends Exception {

    public CommandException(String message) {
        super(message);
    }

    public CommandException() {
        super("Command not found !");
    }
}
