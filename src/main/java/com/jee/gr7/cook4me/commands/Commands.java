package com.jee.gr7.cook4me.commands;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity(name = "Commands")
@Table(name = "Commands",
        indexes = @Index(name = "command_index", columnList = "command_id", unique = true),
        schema = "cook4me_db")
public class Commands {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    @Column(name = "command_id")
    private long commandId;

    @NotNull
    @Column(name = "client_id")
    private long clientId;

    @Column(name = "cook_id")
    private long cookId;

    @NotNull
    private String request;

    @NotNull
    private String state;

    @Column(name = "nb_guest")
    private int nbGuest;

    private int budget;

    private boolean cmdDoneCook;

    private boolean cmdDoneClient;


    public Commands() {
    }


    public long getCommandId() {
        return commandId;
    }

    public void setCommandId(long commandId) {
        this.commandId = commandId;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public long getCookId() {
        return cookId;
    }

    public void setCookId(long cookId) {
        this.cookId = cookId;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getNbGuest() {
        return nbGuest;
    }

    public void setNbGuest(int nbGuest) {
        this.nbGuest = nbGuest;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public boolean isCmdDoneCook() {
        return cmdDoneCook;
    }

    public void setCmdDoneCook(boolean cmdDoneCook) {
        this.cmdDoneCook = cmdDoneCook;
    }

    public boolean isCmdDoneClient() {
        return cmdDoneClient;
    }

    public void setCmdDoneClient(boolean cmdDoneClient) {
        this.cmdDoneClient = cmdDoneClient;
    }

    @Override
    public String toString() {
        return "Commands{" +
                "commandId=" + commandId +
                ", clientId=" + clientId +
                ", cook_id=" + cookId +
                ", request='" + request + '\'' +
                ", state='" + state + '\'' +
                ", nbGuest=" + nbGuest +
                ", budget=" + budget +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Commands)) return false;
        Commands commands = (Commands) o;
        return commandId == commands.commandId &&
                clientId == commands.clientId &&
                cookId == commands.cookId &&
                nbGuest == commands.nbGuest &&
                budget == commands.budget &&
                cmdDoneCook == commands.cmdDoneCook &&
                cmdDoneClient == commands.cmdDoneClient &&
                request.equals(commands.request) &&
                state.equals(commands.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cookId, request, state, nbGuest, budget);
    }
}
