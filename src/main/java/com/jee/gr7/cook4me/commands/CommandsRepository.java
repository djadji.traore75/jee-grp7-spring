package com.jee.gr7.cook4me.commands;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommandsRepository extends JpaRepository<Commands, Long> {

    Optional<Commands> findByCommandId(Long id);

    List<Commands> findAllByClientId(Long id);

    List<Commands> findAllByCookId(Long id);

    List<Commands> findByState(String state);

    List<Commands> findByBudgetGreaterThanAndBudgetLessThan(int a, int b);

}
