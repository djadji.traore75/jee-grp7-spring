package com.jee.gr7.cook4me.commands;


import java.util.Objects;

public class CommandsDto {

    private String clientId;

    private String cookId;

    private String request;

    private String state;

    private String nbGuest;

    private String budget;

    public CommandsDto(String clientId, String cookId, String request, String state, String nbGuest, String budget) {
        this.clientId = clientId;
        this.cookId = cookId;
        this.request = request;
        this.state = state;
        this.nbGuest = nbGuest;
        this.budget = budget;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getCookId() {
        return cookId;
    }

    public void setCookId(String cookId) {
        this.cookId = cookId;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getNbGuest() {
        return nbGuest;
    }

    public void setNbGuest(String nbGuest) {
        this.nbGuest = nbGuest;
    }


    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommandsDto)) return false;
        CommandsDto that = (CommandsDto) o;
        return clientId.equals(that.clientId) &&
                Objects.equals(cookId, that.cookId) &&
                request.equals(that.request) &&
                state.equals(that.state) &&
                Objects.equals(nbGuest, that.nbGuest) &&
                Objects.equals(budget, that.budget);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientId, cookId, request, state, nbGuest, budget);
    }
}
