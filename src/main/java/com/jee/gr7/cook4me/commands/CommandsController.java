package com.jee.gr7.cook4me.commands;

import com.jee.gr7.cook4me.enumeration.RolesEnum;
import com.jee.gr7.cook4me.enumeration.StateEnum;
import com.jee.gr7.cook4me.exception.*;
import com.jee.gr7.cook4me.response.MessageResponse;
import com.jee.gr7.cook4me.users.UserService;
import com.jee.gr7.cook4me.users.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/commands")
public class CommandsController {

    @Autowired
    private CommandsService commandsService;

    @Autowired
    private UserService userService;

    @GetMapping("/all")
    @PreAuthorize("hasRole('ROLE_COOK') or hasRole('ROLE_CLIENT')")
    public ResponseEntity<?> getAllCommandsWithoutCooks() {
        return ResponseEntity.ok(commandsService.getAllCommandWithoutCook());
    }

    @PostMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_COOK')")
    public ResponseEntity<?> cookTakeCommands(Principal principal, @PathVariable("id") Long commandId) {
        if (principal == null)
            return ResponseEntity.badRequest().body(new MessageResponse(UserExceptionEnum.USERDOESNTEXIST.toString()));
        try {
            commandsService.cookTakeCommands(userService.getUserIdFromUsername(principal.getName()), commandId);
        } catch (CommandException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("Commands saved");
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_CLIENT')")
    public ResponseEntity<?> registerCommand(Principal principal, @Valid @RequestBody CommandsDto dto) {
        if (principal == null)
            return ResponseEntity.badRequest().body(new MessageResponse(UserExceptionEnum.USERDOESNTEXIST.toString()));
        try {
            Users user = userService.findByUsername(principal.getName());
            dto.setClientId(user.getUserId().toString());
            dto.setState(StateEnum.PENDING.toString());
            commandsService.saveCommand(dto);
        } catch (CommandException | UserException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("Commands saved");
    }

    @PatchMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_COOK') or hasRole('ROLE_CLIENT')")
    public ResponseEntity<?> updateCommands(@Valid @RequestBody CommandsDto dto, @PathVariable("id") Long commandId) {
        try {
            commandsService.updateCommand(commandId, dto);
            return ResponseEntity.ok(new MessageResponse("Command updated"));
        } catch (CommandException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_COOK') or hasRole('ROLE_CLIENT')")
    public ResponseEntity<?> getCommand(@PathVariable("id") Long commandId) {
        try {
            Commands command = commandsService.getCommand(commandId);
            return ResponseEntity.ok(command);
        } catch (CommandException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_CLIENT')")
    public ResponseEntity<?> deleteCommands(@PathVariable("id") Long commandId) {
        try {
            commandsService.deleteCommands(commandId);
            return ResponseEntity.ok(new MessageResponse("Command deleted"));
        } catch (CommandException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("budget/min={min}/max={max}")
    @PreAuthorize("hasRole('ROLE_COOK') or hasRole('ROLE_CLIENT')")
    public ResponseEntity<?> filterBudget(@PathVariable(name = "min") int min, @PathVariable(name = "max") int max) {
        List<Commands> commands;
        if (min > max)
            return ResponseEntity.badRequest().body(new MessageResponse("Error max value smaller than min value !"));

        commands = commandsService.getCommandsFromBudgetBetween(min, max);
        return ResponseEntity.ok(commands);
    }

    @GetMapping("/state/{state}")
    @PreAuthorize("hasRole('ROLE_COOK') or hasRole('ROLE_CLIENT')")
    public ResponseEntity<?> filterState(@PathVariable(name = "state") String state) {
        if (StateEnum.contains(state))
            return ResponseEntity.ok(commandsService.getAllFromState(state.toUpperCase()));
        return ResponseEntity.badRequest().body(new MessageResponse("State doesnt exist"));
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_COOK') or hasRole('ROLE_CLIENT')")
    public ResponseEntity<?> getPersonalCommands(Authentication authentication, Principal principal) {
        if (authentication == null || principal == null)
            return ResponseEntity.badRequest().body(new MessageResponse(UserExceptionEnum.USERDOESNTEXIST.toString()));
        try {
            return ResponseEntity.ok(commandsService.getCommandForUser(authentication, principal));
        } catch (RoleException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PatchMapping("/{id}/done")
    public ResponseEntity<?> validateCommand(@PathVariable(name = "id") long id, Authentication authentication, Principal principal) {
        if (authentication == null || principal == null)
            return ResponseEntity.badRequest().body(new MessageResponse(UserExceptionEnum.USERDOESNTEXIST.toString()));

        try {
            if (userService.getRoleFromAuthentication(authentication).equalsIgnoreCase(RolesEnum.ROLE_COOK.toString())) {
                return cookValidateCommand(id, principal);
            } else if (userService.getRoleFromAuthentication(authentication).equalsIgnoreCase(RolesEnum.ROLE_CLIENT.toString())) {
                return clientValidateCommand(id, principal);
            }
        } catch (RoleException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.badRequest().body(new MessageResponse(UserExceptionEnum.NOCMDFOUNDFORUSER.toString()));
    }

    private ResponseEntity<?> clientValidateCommand(long id, Principal principal) {
        try {
            commandsService.clientValidateCommand(userService.getUserIdFromUsername(principal.getName()), id);
        } catch (CommandException | CatererException e) {
            ResponseEntity.badRequest().body(new MessageResponse(e.getMessage()));
        }
        return ResponseEntity.ok().build();
    }

    private ResponseEntity<?> cookValidateCommand(long id, Principal principal) {
        try {
            commandsService.cookValidateCommand(userService.getUserIdFromUsername(principal.getName()), id);
        } catch (CommandException | CatererException e) {
            ResponseEntity.badRequest().body(new MessageResponse(e.getMessage()));
        }
        return ResponseEntity.ok().build();
    }

}
