package com.jee.gr7.cook4me.commands;

import com.jee.gr7.cook4me.caterer.CatererService;
import com.jee.gr7.cook4me.enumeration.RolesEnum;
import com.jee.gr7.cook4me.enumeration.StateEnum;
import com.jee.gr7.cook4me.exception.CatererException;
import com.jee.gr7.cook4me.exception.CommandException;
import com.jee.gr7.cook4me.exception.RoleException;
import com.jee.gr7.cook4me.users.UserService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Service
public class CommandsService {

    @Autowired
    private CommandsRepository commandsRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private CatererService catererService;

    public List<Commands> getAllCommandWithoutCook() {
        return commandsRepository.findAllByCookId((long) 0);
    }

    public Commands saveCommand(CommandsDto commandsDto) throws CommandException {
        Commands commands = fromDtoToCommands(commandsDto);
        return commandsRepository.save(commands);
    }

    public Commands fromDtoToCommands(CommandsDto dto) throws CommandException {
        Commands commands = new Commands();
        try {
            commands.setClientId(Long.parseLong(dto.getClientId()));
            if (dto.getCookId() == null || dto.getCookId().equals(""))
                commands.setCommandId(0);
            else
                commands.setCookId(Long.parseLong(dto.getCookId()));
            commands.setRequest(dto.getRequest());
            commands.setState(dto.getState());
            if (!dto.getNbGuest().equals(""))
                commands.setNbGuest(Integer.parseInt(dto.getNbGuest()));
            if (!dto.getBudget().equals(""))
                commands.setBudget(Integer.parseInt(dto.getBudget()));
            return commands;
        } catch (NumberFormatException e) {
            Logger.getLogger(CommandsService.class).error(e.getMessage());
            throw new CommandException("Format error for the commands");
        }
    }

    public void cookTakeCommands(Long cookId, Long commandId) throws CommandException {
        Optional<Commands> command = commandsRepository.findByCommandId(commandId);
        if (!command.isPresent())
            throw new CommandException();
        command.get().setCookId(cookId);
        command.get().setState(StateEnum.ACCEPTED.toString());
        commandsRepository.save(command.get());
    }

    public void updateCommand(Long commandId, CommandsDto dto) throws CommandException {
        Optional<Commands> command = commandsRepository.findByCommandId(commandId);
        if (!command.isPresent())
            throw new CommandException("No command for found");
        if (!StateEnum.contains(dto.getState()))
            throw new CommandException("This state doesnt exist");
        Commands newCommand = fromDtoToCommands(dto);
        newCommand.setCommandId(commandId);
        commandsRepository.save(newCommand);
    }

    public void deleteCommands(Long commandId) throws CommandException {
        Optional<Commands> commands = commandsRepository.findByCommandId(commandId);
        if (commands.isPresent())
            commandsRepository.delete(commands.get());
        else
            throw new CommandException();
    }

    public List<Commands> getCommandsFromBudgetBetween(int a, int b) {
        return commandsRepository.findByBudgetGreaterThanAndBudgetLessThan(a, b);
    }

    public Commands getCommand(long commandId) throws CommandException {
        Optional<Commands> command = commandsRepository.findByCommandId(commandId);
        if (command.isPresent())
            return command.get();
        throw new CommandException();
    }

    public List<Commands> getAllFromState(String state) {
        return commandsRepository.findByState(state);
    }

    private List<Commands> getCookCommands(Long cookId) {
        return commandsRepository.findAllByCookId(cookId);
    }

    private List<Commands> getClientCommands(Long clientId) {
        return commandsRepository.findAllByClientId(clientId);
    }

    public List<Commands> getCommandForUser(Authentication authentication, Principal principal) throws RoleException {
        if (userService.getRoleFromAuthentication(authentication).equalsIgnoreCase(RolesEnum.ROLE_COOK.toString())) {
            return getCookCommands(userService.getUserIdFromUsername(principal.getName()));
        } else if (userService.getRoleFromAuthentication(authentication).equalsIgnoreCase(RolesEnum.ROLE_CLIENT.toString())) {
            return getClientCommands(userService.getUserIdFromUsername(principal.getName()));
        }
        throw new RoleException("Role doesnt exist");
    }

    public void cookValidateCommand(long cookId, long commandId) throws CommandException, CatererException {
        Optional<Commands> commands = commandsRepository.findByCommandId(commandId);
        if (commands.isPresent()) {
            if (commands.get().getCookId() == cookId) {
                commands.get().setCmdDoneCook(true);
                if (checkIfCommandIsNotDone(commands.get()))
                    commandsRepository.save(commands.get());
                return;
            }
            throw new CommandException("This cook is not authorized to change this command state !");
        } else {
            throw new CommandException("No command found for this is id !");
        }
    }

    public void clientValidateCommand(long cookId, long commandId) throws CommandException, CatererException {
        Optional<Commands> commands = commandsRepository.findByCommandId(commandId);
        if (commands.isPresent()) {
            if (commands.get().getClientId() == cookId) {
                commands.get().setCmdDoneClient(true);
                if (checkIfCommandIsNotDone(commands.get()))
                    commandsRepository.save(commands.get());
                return;
            }
            throw new CommandException("This cook is not authorized to change this command state !");
        } else {
            throw new CommandException("No command found for this is id !");
        }
    }

    private boolean checkIfCommandIsNotDone(Commands commands) throws CatererException {
        if (commands.isCmdDoneClient() && commands.isCmdDoneCook()) {
            commands.setState(StateEnum.FINISHED.toString());
            commandsRepository.save(commands);
            catererService.addCommandToCaterer(commands);
            return false;
        }
        return true;
    }
}
