package com.jee.gr7.cook4me.users;

import java.util.Objects;

public class UserDto {

    private String username;

    private String password;

    private String firstName;

    private String lastName;

    private String mail;

    private String role;

    private String phoneNumber;

    public UserDto() {
    }

    public UserDto(String username, String password, String firstName, String lastName, String mail, String role, String phoneNumber) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.role = role;
        this.phoneNumber = phoneNumber;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String email) {
        mail = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", mail='" + mail + '\'' +
                ", role='" + role + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserDto)) return false;
        UserDto userDto = (UserDto) o;
        return username.equals(userDto.username) &&
                password.equals(userDto.password) &&
                firstName.equals(userDto.firstName) &&
                lastName.equals(userDto.lastName) &&
                mail.equals(userDto.mail) &&
                role.equals(userDto.role) &&
                phoneNumber.equals(userDto.phoneNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, firstName, lastName, mail, role, phoneNumber);
    }
}
