package com.jee.gr7.cook4me.users;

import com.jee.gr7.cook4me.enumeration.RolesEnum;
import com.jee.gr7.cook4me.exception.RoleException;
import com.jee.gr7.cook4me.exception.UserException;
import com.jee.gr7.cook4me.exception.UserExceptionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserService {

    @Autowired
    PasswordEncoder encoder;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;

    public void registerUser(UserDto userDto) throws UserException, RoleException {
        if (!isAddressMail(userDto.getMail()))
            throw new UserException(UserExceptionEnum.INVALIDEMAIL + userDto.getMail());

        if (!isPhoneNumber(userDto.getPhoneNumber()))
            throw new UserException(UserExceptionEnum.INVALIDPHONENUMBER.toString() + userDto.getPhoneNumber());

        if (userRepository.findByUsername(userDto.getUsername()).isPresent())
            throw new UserException(UserExceptionEnum.USERNAMETAKEN + userDto.getUsername());
        else {
            if (userRepository.findByMail(userDto.getMail()).isPresent()) {
                throw new UserException(UserExceptionEnum.MAILTAKEN + userDto.getMail());
            }
        }

        Users user;
        try {
            user = fromUserDTOToUsers(userDto);
        } catch (RoleException e) {
            throw new RoleException("Role doesnt exist : " + userDto.getRole());
        }
        assert (user != null);
        Logger.getAnonymousLogger().info(user.toString());
        userRepository.save(user);
    }


    public Users fromUserDTOToUsers(UserDto userDto) throws RoleException {
        Users user = new Users();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());

        user.setEnabled(true);
        user.setMail(userDto.getMail());
        user.setUsername(userDto.getUsername());
        user.setPassword(encoder.encode(userDto.getPassword()));
        user.setPhoneNumber(userDto.getPhoneNumber());

        if (userDto.getRole().equalsIgnoreCase("client") ||
                userDto.getRole().equalsIgnoreCase(RolesEnum.ROLE_CLIENT.toString())) {
            user.setRoles(fndRoleWithName(RolesEnum.ROLE_CLIENT.toString()));
        } else if (userDto.getRole().equalsIgnoreCase("cook") ||
                userDto.getRole().equalsIgnoreCase(RolesEnum.ROLE_COOK.toString())) {
            user.setRoles(fndRoleWithName(RolesEnum.ROLE_COOK.toString()));
        } else {
            throw new RoleException("Role not found");
        }
        return user;
    }

    public UserDto fromUsersToUserDTO(Users users) {
        UserDto userDto = new UserDto();
        userDto.setFirstName(users.getFirstName());
        userDto.setLastName(users.getLastName());
        userDto.setMail(users.getMail());
        userDto.setUsername(users.getUsername());
        userDto.setPassword(users.getPassword());
        userDto.setPhoneNumber(users.getPhoneNumber());
        userDto.setRole(users.getRoles().getRoleName());
        return userDto;
    }

    public Roles fndRoleWithName(String name) throws RoleException {
        Optional<Roles> role = roleRepository.findByRoleName(name);
        if (role.isPresent()) {
            return role.get();
        } else {
            throw new RoleException("Role doesnt exist : " + name);
        }
    }

    public Users findByUsername(String username) throws UserException {
        Optional<Users> user = userRepository.findByUsername(username);
        if (user.isPresent())
            return user.get();
        throw new UserException(UserExceptionEnum.USERDOESNTEXIST.toString() + username);
    }

    public void delete(Users users) {
        userRepository.delete(users);
    }

    public Boolean isAddressMail(String mail) {
        Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher m = p.matcher(mail);
        return m.matches();
    }

    public Boolean isPhoneNumber(String number) {
        Pattern p = Pattern.compile("^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$");
        Matcher m = p.matcher(number);
        return m.matches();
    }


    public void cleanUpRepository(String username) {
        try {
            userRepository.delete(findByUsername(username));
        } catch (UserException e) {
            Logger.getAnonymousLogger().warning(e.getMessage());
        }
    }


    public long getUserIdFromUsername(String username) {
        Optional<Users> user = userRepository.findByUsername(username);
        if (user.isPresent())
            return user.get().getUserId();
        return -1;
    }

    public String getRoleFromAuthentication(Authentication authentication) throws RoleException {
        Collection<? extends GrantedAuthority> authorities
                = authentication.getAuthorities();
        for (GrantedAuthority grantedAuthority : authorities) {
            if (grantedAuthority.getAuthority().equals(RolesEnum.ROLE_CLIENT.toString())) {
                return RolesEnum.ROLE_CLIENT.toString();
            } else if (grantedAuthority.getAuthority().equals(RolesEnum.ROLE_COOK.toString())) {
                return RolesEnum.ROLE_COOK.toString();
            }
        }
        throw new RoleException("Role not found for this user");
    }


    public Optional<UserInfo> getUserInfo(long id) {
        Optional<Users> user = userRepository.findById(id);
        return user.map(users -> new UserInfo(users.getUsername(), users.getFirstName(), users.getLastName(), users.getMail(), users.getPhoneNumber()));
    }

}
