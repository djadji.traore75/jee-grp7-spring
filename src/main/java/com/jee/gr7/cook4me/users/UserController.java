package com.jee.gr7.cook4me.users;

import com.jee.gr7.cook4me.exception.UserException;
import com.jee.gr7.cook4me.exception.UserExceptionEnum;
import com.jee.gr7.cook4me.response.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Optional;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @DeleteMapping
    public ResponseEntity<?> deleteUser(Principal principal) {
        if (principal == null)
            return ResponseEntity.badRequest().body(new MessageResponse(UserExceptionEnum.USERDOESNTEXIST.toString()));
        Users user;
        try {
            user = userService.findByUsername(principal.getName());
            userService.delete(user);
            return ResponseEntity.ok(new MessageResponse(UserExceptionEnum.USERDELETEDSUCCESFULLY.toString()));
        } catch (UserException e) {
            return ResponseEntity.notFound().build();

        }
    }

    @GetMapping(value = {"", "{id}"})
    public ResponseEntity<?> getUserInfo(Principal principal, @PathVariable(name = "id", required = false) Integer id) {
        if (principal == null)
            return ResponseEntity.badRequest().body(new MessageResponse(UserExceptionEnum.USERDOESNTEXIST.toString()));
        Users user;
        try {
            if (id == null) {
                user = userService.findByUsername(principal.getName());
                Optional<UserInfo> userInfo = userService.getUserInfo(user.getUserId());
                return ResponseEntity.ok(userInfo);
            } else {
                Optional<UserInfo> userInfo = userService.getUserInfo(id);
                if (userInfo.isPresent())
                    return ResponseEntity.ok(userInfo.get());
                else
                    return ResponseEntity.notFound().build();
            }
        } catch (UserException e) {
            return ResponseEntity.badRequest().body(new MessageResponse(e.getMessage()));

        }
    }

}
