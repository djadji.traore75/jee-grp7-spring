package com.jee.gr7.cook4me.users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {

    Optional<Users> findByUsername(String username);

    Optional<Users> findByMail(String mail);

    Boolean existsByUsername(String username);

    Boolean existsByMail(String mail);
}
