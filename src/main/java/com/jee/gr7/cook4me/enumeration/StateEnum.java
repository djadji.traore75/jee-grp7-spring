package com.jee.gr7.cook4me.enumeration;

public enum StateEnum {
    PENDING,
    ACCEPTED,
    IN_PREPARATION,
    READY,
    FINISHED;

    public static boolean contains(String test) {

        for (StateEnum state : StateEnum.values()) {
            if (state.name().equalsIgnoreCase(test)) {
                return true;
            }
        }
        return false;
    }
}
