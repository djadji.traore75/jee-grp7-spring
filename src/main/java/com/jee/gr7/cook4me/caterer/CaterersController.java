package com.jee.gr7.cook4me.caterer;

import com.jee.gr7.cook4me.caterer.dish.DishesDto;
import com.jee.gr7.cook4me.exception.CatererException;
import com.jee.gr7.cook4me.exception.DishesException;
import com.jee.gr7.cook4me.users.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/caterer")
public class CaterersController {

    @Autowired
    private CatererService catererService;

    @Autowired
    private UserService userService;

    @GetMapping("/{catererId}")
    public ResponseEntity<?> getCaterer(@PathVariable(name = "catererId") long id) {
        try {
            Caterers caterer = catererService.getCatererById(id);
            return ResponseEntity.ok(caterer);
        } catch (CatererException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PreAuthorize("hasRole('ROLE_COOK')")
    @PostMapping
    public ResponseEntity<?> saveCaterer(@Valid @RequestBody CatererDto catererDto, Principal principal) {
        try {
            catererService.saveCaterer(catererDto, userService.getUserIdFromUsername(principal.getName()));
            return ResponseEntity.ok("Caterer save !");
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PreAuthorize("hasRole('ROLE_COOK')")
    @PostMapping("/{catererId}/dishes")
    public ResponseEntity<?> saveNewDishForCaterer(@Valid @RequestBody DishesDto dishesDto, @PathVariable("catererId") long catererId) {
        try {
            catererService.addDishToCaterer(dishesDto, catererId);
            return ResponseEntity.ok("Dish save !");
        } catch (CatererException e) {
            return ResponseEntity.notFound().build();
        }
    }


    @PreAuthorize("hasRole('ROLE_COOK')")
    @PatchMapping("/{catererId}/dishes/{dishId}")
    public ResponseEntity<?> updateDish(@Valid @RequestBody DishesDto dishesDto, @PathVariable("catererId") long catererId,
                                        @PathVariable("dishId") long dishId) {

        try {
            catererService.updateDish(dishesDto, catererId, dishId);
            return ResponseEntity.ok("Dish updated");
        } catch (CatererException | DishesException e) {
            return ResponseEntity.notFound().build();
        }
    }


}
