package com.jee.gr7.cook4me.caterer.dish;

public class DishesDto {
    private String name;

    private String description;

    private String allergens;

    public DishesDto(String name, String description, String allergens) {
        this.name = name;
        this.description = description;
        this.allergens = allergens;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAllergens() {
        return allergens;
    }

    public void setAllergens(String allergens) {
        this.allergens = allergens;
    }

}
