package com.jee.gr7.cook4me.caterer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CaterersRepository extends JpaRepository<Caterers, Long> {

    Optional<Caterers> findByCatererId(long id);

    @Override
    List<Caterers> findAll();

    Optional<Caterers> findByCookId(long cookID);
}
