package com.jee.gr7.cook4me.caterer.dish;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jee.gr7.cook4me.caterer.Caterers;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity(name = "dishes")
@Table(name = "dishes",
        indexes = {@Index(name = "dishes_index", columnList = "dish_id", unique = true)},
        schema = "cook4me_db")
public class Dishes {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    @Column(name = "dish_id")
    private long dishId;

    @Column(name = "caterer_id")
    private long catererId;

    private String name;

    private String description;

    private String allergens;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinTable(
            name = "caterers_dishes",
            joinColumns = {@JoinColumn(name = "dish_id", referencedColumnName = "dish_id")},
            inverseJoinColumns = {@JoinColumn(name = "caterer_id", referencedColumnName = "caterer_id")})
    private Caterers caterer;

    public Dishes() {
    }

    public Dishes(long catererId, String name, String description, String allergens) {
        this.name = name;
        this.description = description;
        this.allergens = allergens;
        this.catererId = catererId;
    }

    public Dishes(long catererId, String name, String description, String allergens, Caterers caterer) {
        this.name = name;
        this.description = description;
        this.allergens = allergens;
        this.catererId = catererId;
        this.caterer = caterer;
    }

    public long getDishId() {
        return dishId;
    }

    public void setDishId(long dishId) {
        this.dishId = dishId;
    }

    public long getCatererId() {
        return catererId;
    }

    public void setCatererId(long catererId) {
        this.catererId = catererId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAllergens() {
        return allergens;
    }

    public void setAllergens(String allergens) {
        this.allergens = allergens;
    }

    public Caterers getCaterer() {
        return caterer;
    }

    public void setCaterer(Caterers caterer) {
        this.caterer = caterer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dishes)) return false;
        Dishes dishes = (Dishes) o;
        return dishId == dishes.dishId &&
                catererId == dishes.catererId &&
                name.equals(dishes.name) &&
                Objects.equals(caterer, dishes.caterer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dishId, catererId, name, caterer);
    }
}
