package com.jee.gr7.cook4me.caterer;

import com.jee.gr7.cook4me.caterer.dish.Dishes;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "caterers")
@Table(name = "caterers",
        indexes = {@Index(name = "caterers_index", columnList = "caterer_id", unique = true),
                @Index(name = "caterers_cook_index", columnList = "cook_id", unique = true)},
        schema = "cook4me_db")
public class Caterers {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    @Column(name = "caterer_id")
    private long catererId;

    @Column(name = "cook_id")
    private long cookId;

    private String name;

    private String description;

    @Column(name = "nb_command")
    private int nbCommands;

    private long revenue;

    @OneToMany(mappedBy = "caterer")
    private List<Dishes> dishesList;

    public Caterers() {
    }

    public Caterers(long cookId, String name, String description, int nbCommands, long revenue) {
        this.cookId = cookId;
        this.name = name;
        this.description = description;
        this.nbCommands = nbCommands;
        this.revenue = revenue;
    }

    public Caterers(long cookId, String name, String description, List<Dishes> dishesList) {
        this.cookId = cookId;
        this.name = name;
        this.description = description;
        this.revenue = 0;
        this.nbCommands = 0;
        this.dishesList = dishesList;
    }

    public Caterers(long cookId, String name, String description) {
        this.cookId = cookId;
        this.name = name;
        this.description = description;
        this.revenue = 0;
        this.nbCommands = 0;
        this.dishesList = new ArrayList<>();
    }

    public long getCatererId() {
        return catererId;
    }

    public void setCatererId(long catererId) {
        this.catererId = catererId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNbCommands() {
        return nbCommands;
    }

    public void setNbCommands(int nbCommands) {
        this.nbCommands = nbCommands;
    }

    public long getRevenue() {
        return revenue;
    }

    public void setRevenue(long revenue) {
        this.revenue = revenue;
    }

    public long getCookId() {
        return cookId;
    }

    public void setCookId(long cookId) {
        this.cookId = cookId;
    }

    public List<Dishes> getDishesList() {
        return dishesList;
    }

    public void setDishesList(List<Dishes> dishesList) {
        this.dishesList = dishesList;
    }

    @Override
    public String toString() {
        return "Caterers{" +
                "catererId=" + catererId +
                ", cookId=" + cookId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", nbCommands=" + nbCommands +
                ", revenue=" + revenue +
                ", dishesList=" + dishesList +
                '}';
    }

}
