package com.jee.gr7.cook4me.caterer;

import com.jee.gr7.cook4me.caterer.dish.Dishes;
import com.jee.gr7.cook4me.caterer.dish.DishesDto;
import com.jee.gr7.cook4me.caterer.dish.DishesRepository;
import com.jee.gr7.cook4me.commands.Commands;
import com.jee.gr7.cook4me.exception.CatererException;
import com.jee.gr7.cook4me.exception.DishesException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CatererService {

    @Autowired
    private CaterersRepository caterersRepository;

    @Autowired
    private DishesRepository dishesRepository;

    public void saveCaterer(CatererDto catererDto, long cookId) {
        catererDto.setCookId(cookId);
        saveCatererDto(catererDto);
    }

    private void saveCatererDto(CatererDto catererDto) {
        caterersRepository.save(new Caterers(catererDto.getCookId(), catererDto.getName(), catererDto.getDescription()));
    }

    public void addCommandToCaterer(Commands commands) throws CatererException {
        Optional<Caterers> caterer = caterersRepository.findByCookId(commands.getCookId());
        if (caterer.isPresent()) {
            caterer.get().setNbCommands(caterer.get().getNbCommands() + 1);
            caterer.get().setRevenue(caterer.get().getRevenue() + commands.getBudget());
            caterersRepository.save(caterer.get());
        } else {
            throw new CatererException("Caterer doesn't exist !");
        }
    }


    public void addDishToCaterer(DishesDto dto, long catererId) throws CatererException {
        Optional<Caterers> caterer = caterersRepository.findByCatererId(catererId);
        if (caterer.isPresent()) {
            Dishes dish = fromDishDtoToDish(dto, caterer.get());
            dishesRepository.save(dish);
        } else {
            throw new CatererException("Caterer doesn't exist !");
        }
    }

    private Dishes fromDishDtoToDish(DishesDto dishesDto, Caterers caterers) {
        return new Dishes(caterers.getCatererId(), dishesDto.getName(), dishesDto.getDescription(), dishesDto.getAllergens(), caterers);
    }

    public Caterers getCatererById(long id) throws CatererException {
        Optional<Caterers> catererById = caterersRepository.findByCatererId(id);
        if (catererById.isPresent()) {
            Caterers caterer = catererById.get();
            return filterCatererInDishes(caterer);
        } else {
            throw new CatererException("No caterer found for this id !");
        }
    }

    private Caterers filterCatererInDishes(Caterers caterer) {
        for (Dishes dishes : caterer.getDishesList()) {
            dishes.setCaterer(null);
        }
        return caterer;
    }


    public void updateDish(DishesDto dishDto, long catererId, long dishId) throws CatererException, DishesException {
        Optional<Caterers> caterer = caterersRepository.findByCatererId(catererId);
        if (caterer.isPresent()) {
            Optional<Dishes> dishInDb = dishesRepository.findByDishId(dishId);
            if (!dishInDb.isPresent())
                throw new DishesException("This dishes doesnt exist !");

            List<Dishes> dishesList = caterer.get().getDishesList();
            if (!dishesList.contains(dishInDb.get())) {
                throw new DishesException("Dish not in this caterer !");
            }

            dishInDb.get().setDescription(dishDto.getDescription());
            dishInDb.get().setAllergens(dishDto.getAllergens());
            dishInDb.get().setName(dishDto.getName());
            dishesRepository.save(dishInDb.get());
        } else
            throw new CatererException("Caterer doesnt exist !");
    }

}
