package com.jee.gr7.cook4me.caterer;

import com.jee.gr7.cook4me.caterer.dish.DishesDto;

import java.util.List;

public class CatererDto {

    private long cookId;

    private String name;

    private String description;

    private int nbCommands;

    private List<DishesDto> dishesList;

    public CatererDto(long cookId, String name, String description, int nbCommands, List<DishesDto> dishesList) {
        this.cookId = cookId;
        this.name = name;
        this.description = description;
        this.nbCommands = nbCommands;
        this.dishesList = dishesList;
    }

    public long getCookId() {
        return cookId;
    }

    public void setCookId(long cookId) {
        this.cookId = cookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNbCommands() {
        return nbCommands;
    }

    public void setNbCommands(int nbCommands) {
        this.nbCommands = nbCommands;
    }

    public List<DishesDto> getDishesList() {
        return dishesList;
    }

    public void setDishesList(List<DishesDto> dishesList) {
        this.dishesList = dishesList;
    }
}
