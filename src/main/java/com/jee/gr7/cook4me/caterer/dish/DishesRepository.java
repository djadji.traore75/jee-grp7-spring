package com.jee.gr7.cook4me.caterer.dish;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DishesRepository extends JpaRepository<Dishes, Long> {

    List<Dishes> findAllByCatererId(long id);

    Optional<Dishes> findByDishId(long id);

    List<Dishes> findAllByNameContains(String search);

    Optional<Dishes> getByDishId(Long id);

}
