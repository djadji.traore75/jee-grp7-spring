import com.jee.gr7.cook4me.Cook4meApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = Cook4meApplication.class)
@EnableAutoConfiguration
class Cook4meApplicationTests {

    @Test
    void contextLoads() {
    }

}
