package com.jee.gr7.cook4me.commands;

import com.jee.gr7.cook4me.enumeration.StateEnum;
import com.jee.gr7.cook4me.exception.CommandException;
import com.jee.gr7.cook4me.exception.UserException;
import com.jee.gr7.cook4me.users.UserService;
import com.jee.gr7.cook4me.users.Users;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Configuration
public class CommandsServiceTest {

    @Autowired
    CommandsService commandsService;

    @Autowired
    UserService userService;

    @Test
    public void no_error_when_saving_request() throws CommandException, UserException {

        Users client = userService.findByUsername("client");
        commandsService.saveCommand(new CommandsDto(String.valueOf(client.getUserId()), "", "request test",
                StateEnum.PENDING.toString(), String.valueOf(10), String.valueOf(250)));

    }

    @Test(expected = CommandException.class)
    public void exception_when_saving_request_with_bad_number_budget() throws CommandException, UserException {

        Users client = userService.findByUsername("client");
        commandsService.saveCommand(new CommandsDto(String.valueOf(client.getUserId()), "", "request test",
                StateEnum.PENDING.toString(), String.valueOf(10), "250 atete"));
    }


    @Test(expected = CommandException.class)
    public void exception_when_saving_request_with_bad_number_nbGuest() throws CommandException, UserException {

        Users client = userService.findByUsername("client");
        commandsService.saveCommand(new CommandsDto(String.valueOf(client.getUserId()), "", "request test",
                StateEnum.PENDING.toString(), "10er", String.valueOf(250)));
    }

    @Test(expected = CommandException.class)
    public void exception_when_saving_request_with_bad_number_client_id() throws CommandException {
        commandsService.saveCommand(new CommandsDto("test", "", "request test",
                StateEnum.PENDING.toString(), String.valueOf(10), String.valueOf(250)));
    }

    @Test(expected = CommandException.class)
    public void exception_when_saving_request_with_bad_number_cook_id() throws CommandException, UserException {

        Users client = userService.findByUsername("client");
        commandsService.saveCommand(new CommandsDto(String.valueOf(client.getUserId()), "test", "request test",
                StateEnum.PENDING.toString(), String.valueOf(10), String.valueOf(250)));
    }


    @Test
    public void no_cmd_with_cook_in_getAllWithoutCook() throws CommandException, UserException {
        Users client = userService.findByUsername("client");
        commandsService.saveCommand(new CommandsDto(String.valueOf(client.getUserId()), "", "request test no cook",
                StateEnum.PENDING.toString(), String.valueOf(10), String.valueOf(250)));

        List<Commands> commandsList = commandsService.getAllCommandWithoutCook();

        for (Commands command : commandsList) {
            if (command.getCookId() != 0)
                Assert.fail();
        }
    }

    @Test
    public void no_error_when_cook_take_command() throws CommandException, UserException {
        Users client = userService.findByUsername("client");
        Users cook = userService.findByUsername("cook");
        Commands command = commandsService.saveCommand(new CommandsDto(String.valueOf(client.getUserId()), "", "request test cook",
                StateEnum.PENDING.toString(), String.valueOf(10), String.valueOf(250)));

        commandsService.cookTakeCommands(cook.getUserId(), command.getCommandId());
    }

    @Test(expected = CommandException.class)
    public void exception_when_cook_take__no_existing_command()
            throws CommandException, UserException {
        Users cook = userService.findByUsername("cook");

        commandsService.cookTakeCommands(cook.getUserId(), (long) 0);
    }

    @Test(expected = CommandException.class)
    public void exception_when_updating_non_existing_command() throws CommandException {
        CommandsDto commandsDto = new CommandsDto(String.valueOf(0), "", "request test cook",
                StateEnum.PENDING.toString(), String.valueOf(10), String.valueOf(250));
        commandsService.updateCommand(0L, commandsDto);
    }


    @Test(expected = CommandException.class)
    public void exception_when_deleting_non_existing_command() throws CommandException {
        CommandsDto commandsDto = new CommandsDto(String.valueOf(0), "", "request test cook",
                StateEnum.PENDING.toString(), String.valueOf(10), String.valueOf(250));
        commandsService.updateCommand(0L, commandsDto);
    }


    @Test
    public void delete_command() throws CommandException, UserException {
        Users client = userService.findByUsername("client");
        Commands command = commandsService.saveCommand(new CommandsDto(String.valueOf(client.getUserId()), "", "request test cook",
                StateEnum.PENDING.toString(), String.valueOf(10), String.valueOf(250)));

        commandsService.deleteCommands(command.getCommandId());
    }


}
