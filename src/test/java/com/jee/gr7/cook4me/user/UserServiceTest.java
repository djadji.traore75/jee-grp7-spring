package com.jee.gr7.cook4me.user;

import com.jee.gr7.cook4me.enumeration.RolesEnum;
import com.jee.gr7.cook4me.exception.RoleException;
import com.jee.gr7.cook4me.exception.UserException;
import com.jee.gr7.cook4me.users.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@EnableAutoConfiguration
public class UserServiceTest {

    String username = "testUserService";

    @Autowired
    UserService userService;
    @Autowired
    RoleRepository roleRepository;

    @Test
    public void fromUserDTOToUsersTest() throws RoleException {
        Users users = new Users(username, "test", "test",
                "test", "test@mail", "01230516158", userService.fndRoleWithName(RolesEnum.ROLE_CLIENT.toString()));
        Users userTest = userService.fromUserDTOToUsers(new UserDto(username, "test", "test",
                "test", "test@mail", RolesEnum.ROLE_CLIENT.toString(), "01230516158"));

        assertEquals(users, userTest);
    }

    @Test
    public void fromUserToUsersDTOTest() throws RoleException {
        UserDto userDto = new UserDto("test", "test", "test", "test", "test@mail",
                RolesEnum.ROLE_CLIENT.toString(), "01230516158");
        UserDto userDtoTest = userService.fromUsersToUserDTO(new Users("test", "test", "test",
                "test", "test@mail", "01230516158", userService.fndRoleWithName(RolesEnum.ROLE_CLIENT.toString())));
        assertEquals(userDto, userDtoTest);
    }

    @Test(expected = UserException.class)
    public void registerUserMailException() throws RoleException, UserException {
        UserDto userDto = new UserDto("test", "test", "test", "test", "test@mail",
                RolesEnum.ROLE_CLIENT.toString(), "01230516158");
        userService.registerUser(userDto);
    }


    @Test(expected = UserException.class)
    public void registerUserPhoneNumberException() throws RoleException, UserException {
        UserDto userDto = new UserDto("test", "test", "test", "test", "test@mail.com", "client", "0x0x0x0x0");
        userService.registerUser(userDto);
    }


    @Test(expected = RoleException.class)
    public void exception_when_fndRoleWithName_with_non_existing_role() throws RoleException {
        userService.fndRoleWithName("fakerole");
    }

    @Test
    public void find_role_with_RoleWithName_with_role_client() throws RoleException {
        Roles roleWithService = userService.fndRoleWithName(RolesEnum.ROLE_CLIENT.toString());
        Optional<Roles> roleWithRepository = roleRepository.findByRoleName(RolesEnum.ROLE_CLIENT.toString());
        if (roleWithRepository.isPresent())
            assertEquals(roleWithService, roleWithRepository.get());
        else
            Assert.fail();
    }

    @Test
    public void find_role_with_RoleWithName_with_role_cook() throws RoleException {
        Roles roleWithService = userService.fndRoleWithName(RolesEnum.ROLE_COOK.toString());
        Optional<Roles> roleWithRepository = roleRepository.findByRoleName(RolesEnum.ROLE_COOK.toString());
        if (roleWithRepository.isPresent()) {
            assertEquals(roleWithService, roleWithRepository.get());
        } else
            Assert.fail();
    }


}

