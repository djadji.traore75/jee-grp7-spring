package com.jee.gr7.cook4me.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jee.gr7.cook4me.enumeration.RolesEnum;
import com.jee.gr7.cook4me.security.jwt.JwtUtils;
import com.jee.gr7.cook4me.users.UserDto;
import com.jee.gr7.cook4me.users.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@ContextConfiguration
@AutoConfigureMockMvc
@SpringBootTest
@EnableAutoConfiguration
public class UserControllerTest {

    String username = "testmockMVC";
    String test = "test";

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserService userService;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUpUserControllerTest() {
        initMocks(this);
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @After
    public void cleanUpUserServiceTest() {
        userService.cleanUpRepository(username);
    }

    @Test
    public void is_ok_when_register_user() throws Exception {
        userService.cleanUpRepository(username);

        UserDto userDto = new UserDto(username, "test", "test",
                "test", "testmockMVC@mail.com", RolesEnum.ROLE_COOK.toString(), "0140334561");

        this.mockMvc.perform(post("/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userDto))
                .characterEncoding("utf-8"))
                .andExpect(status().isOk());

    }

    @Test
    public void bad_request_from_bad_phoneNumber_register_user() throws Exception {
        UserDto userDto = new UserDto(test, test, test,
                test, "test@mail.com", "client", "0x0x0x0x0");

        this.mockMvc.perform(post("/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userDto))
                .characterEncoding("utf-8"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void bad_request_from_bad_mail_register_user() throws Exception {
        UserDto userDto = new UserDto(test, test, test,
                test, "test@mail", "client", "0140336656");

        this.mockMvc.perform(post("/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userDto))
                .characterEncoding("utf-8"))
                .andExpect(status().isBadRequest());
    }
}
